from flask import Flask

from .config import config
from .controllers import ApiController, EventController, ParticipantController
from .database import Database

app = Flask(__name__)

app.config.update(SQLALCHEMY_TRACK_MODIFICATIONS=True)
database = Database(app, config.with_namespace('database'))

ApiController(app)
EventController()
ParticipantController()
