from flask_restx import Resource
from flask_restx.reqparse import RequestParser

from ..database import db, Event, Participant
from ..database.models.participant import Availability
from ..util import validated_string

from .api import ApiController


parser = RequestParser()
parser.add_argument('availabilities', type=Availability, action='append',
                    required=True, nullable=False)

post_parser = parser.copy()
post_parser.add_argument('name', type=validated_string(255), required=True,
                         nullable=False)


class ParticipantController:
    def __init__(self):
        @ApiController.instance.api.route('/<validated_string(8):event_id>')
        class ParticipantListApi(Resource):
            def post(self, event_id):
                args = post_parser.parse_args(strict=True)
                if event := Event.query.get(event_id):
                    diff = event.end_time - event.start_time
                    if len(event.availabilities) == diff.hours * 2 + \
                            (1 if diff.minutes else 0):
                        participant = Participant(
                            name=args['name'],
                            availabilities=args['availabilities']
                        )
                        event.participants.append(participant)
                        db.session.commit()
                        return participant, 201
                    else:
                        return 'availabilities is an invalid length', 400
                else:
                    return 'event {} does not exist'.format(event_id), 404

        @ApiController.instance.api.route(
            '/<validated_string(8):event_id>/' +
            '<validated_string(255):participant_name>'
        )
        class ParticipantApi(Resource):
            def get(self, event_id, participant_name):
                if participant := Participant.query.get(name=participant_name,
                                                        event_id=event_id):
                    return participant, 200
                else:
                    return 'participant {} in event {} does not exist' \
                        .format(participant_name, event_id), 404

            def put(self, event_id, participant_name):
                if participant := Participant.query.get(name=participant_name,
                                                        event_id=event_id):
                    args = parser.parse_args(strict=True)
                    event = participant.event
                    diff = event.end_time - event.start_time
                    if len(event.availabilities) == \
                            diff.hours * 2 + (1 if diff.minutes else 0):
                        participant.availabilities = args['availabilities']
                        db.session.commit()
                        return participant, 200
                    else:
                        return 'availabilities is an invalid length', 400
                else:
                    return 'participant {} in event {} does not exist' \
                        .format(participant_name, event_id), 404

            def delete(self, event_id, participant_name):
                if participant := Participant.query.get(name=participant_name,
                                                        event_id=event_id):
                    db.session.delete(participant)
                    db.session.commit()
                    return 'participant {} in event {} deleted' \
                        .format(participant_name, event_id), 204
                else:
                    return 'participant {} in event {} does not exist' \
                        .format(participant_name, event_id), 404
