from dateutil.tz import gettz
from flask_restx import Resource
from flask_restx.inputs import date_from_iso8601, datetime_from_iso8601
from flask_restx.reqparse import RequestParser

from .api import ApiController
from ..database import db, Event
from ..util import validated_string

from datetime import datetime, timedelta


def deletion_datetime(days):
    num_days = int(days)
    assert 1 <= num_days <= 365
    return datetime.now() + timedelta(days=num_days)


parser = RequestParser()
parser.add_argument('name', type=validated_string(8), required=True,
                    nullable=False)
parser.add_argument('start_time', type=datetime_from_iso8601, required=True,
                    nullable=False)
parser.add_argument('end_time', type=datetime_from_iso8601, required=True,
                    nullable=False)
parser.add_argument('timezone', type=gettz, required=True, nullable=False)
parser.add_argument('delete_time', type=deletion_datetime, required=True,
                    nullable=False)
parser.add_argument('dates', type=date_from_iso8601, action='append',
                    required=True, nullable=False)


class EventController:
    def __init__(self):
        @ApiController.instance.api.route('/')
        class EventListApi(Resource):
            def post(self):
                args = parser.parse_args(strict=True)
                timezone = args['timezone']
                event = Event(
                    name=args['name'],
                    start_time=args['start_time'].astimezone(timezone),
                    end_time=args['end_time'].astimezone(timezone),
                    delete_time=args['delete_time'], dates=args['dates']
                )
                db.session.add(event)
                db.session.commit()
                return event, 201

        @ApiController.instance.api.route('/<validated_string(8):event_id>')
        class EventApi(Resource):
            def get(self, event_id):
                if event := Event.query.get(event_id):
                    return event, 200
                else:
                    return 'event {} does not exist'.format(event_id), 404

            def delete(self, event_id):
                if event := Event.query.get(event_id):
                    db.session.delete(event)
                    db.session.commit()
                    return 'event {} deleted'.format(event_id), 204
                else:
                    return 'event {} does not exist'.format(event_id), 404
