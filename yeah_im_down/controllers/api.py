from flask_restx import Api


class ApiController:
    instance = None

    def __init__(self, app):
        ApiController.instance = self
        self.api = Api(app)
