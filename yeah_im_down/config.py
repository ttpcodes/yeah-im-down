from everett.manager import ConfigManager, ConfigOSEnv


config = ConfigManager(environments=[
    ConfigOSEnv()
])
