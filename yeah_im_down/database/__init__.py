from everett.manager import Option

from .database import db
from .models import Event, Participant

from urllib.parse import quote_plus


class Database:
    class Config():
        host = Option(doc='postgres connection host', parser=quote_plus,
                      default='localhost')
        port = Option(doc='postgres connection port', parser=quote_plus,
                      default='5432')
        username = Option(doc='postgres connection username',
                          parser=quote_plus, default='yeah_im_down')
        password = Option(doc='postgres connection password',
                          parser=quote_plus, default='yeah_im_down')
        database = Option(doc='postgres database name', parser=quote_plus,
                          default='yeah_im_down')

    def __init__(self, app, config):
        self.config = config.with_options(self)
        app.config.update(
            SQLALCHEMY_DATABASE_URI='postgresql://{}:{}@{}:{}/{}'.format(
                self.config('username'), self.config('password'),
                self.config('host'), self.config('port'),
                self.config('database')
            ))

        db.init_app(app)
        with app.app_context():
            db.create_all()
