from flask_sqlalchemy import Model, SQLAlchemy
from sqlalchemy import Column, DateTime, func


class Base(Model):
    created_at = Column(DateTime(), nullable=False, server_default=func.now())
    updated_at = Column(DateTime(), nullable=False, onupdate=func.now())


db = SQLAlchemy(model_class=Base)
