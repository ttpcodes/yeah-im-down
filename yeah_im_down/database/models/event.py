from sqlalchemy.orm import validates

from ..database import db

from datetime import timedelta
from secrets import randbelow


def random_unique_id():
    id_ = randbelow(2**31)
    while Event.query.get(id_):
        id_ = randbelow(2**31)
    return id_


class Event(db.Model):
    _id = db.Column(db.Integer(), primary_key=True, default=random_unique_id)
    name = db.Column(db.String(255), nullable=False)
    start_time = db.Column(db.Time(timezone=True), nullable=False)
    end_time = db.Column(db.Time(timezone=True), nullable=False)
    delete_time = db.Column(db.DateTime(timezone=True), nullable=False)
    dates = db.Column(db.ARRAY(db.Date()), nullable=False)
    participants = db.relationship('Participant', backref='event')

    @property
    def id_(self):
        return format(self._id, '08x')

    @property
    def timezone(self):
        return self.start_time

    @validates('start_time', 'end_time')
    def validate_start_time(self, key, time):
        assert time.tzinfo == (self.end_time.tzinfo if key == 'start_time'
                               else self.start_time.tzinfo)

    @validates('dates')
    def validate_dates(self, _, dates):
        assert 1 <= len(dates) <= 28

    @validates('delete_time')
    def validate_delete_time(self, _, delete_time):
        assert timedelta(0) <= (delete_time - self.created_at).days <= \
            timedelta(days=365)
