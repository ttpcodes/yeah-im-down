from sqlalchemy.orm import validates

from ..database import db

from enum import Enum


class Availability(Enum):
    UNAVAILABLE = 0
    AVAILABLE = 1
    LIKELY = 2
    UNCERTAIN = 3
    UNLIKELY = 4


class Participant(db.Model):
    name = db.Column(db.String(255), primary_key=True)
    event_id = db.Column(db.Integer(), db.ForeignKey('event._id'),
                         primary_key=True)

    availabilities = db.Column(db.ARRAY(db.Enum(Availability)))

    @validates('availabilities')
    def validate_availabilities(self, _, availabilities):
        diff = self.event.end_time - self.event.start_time
        assert len(availabilities) == diff.hours * 2 + (1 if diff.minutes else
                                                        0)
