def validated_string(max_len):
    def string_validator(string):
        assert len(string) < 255
        return string
